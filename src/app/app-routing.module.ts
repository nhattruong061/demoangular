import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { DropdownComponent } from './component/content/dropdown/dropdown.component';
import { AccordionComponent } from './component/content/accordion/accordion.component';
const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: 'dropdown', component: DropdownComponent },
      { path: 'accordion', component: AccordionComponent },
    ]
  },
  { path: '', pathMatch: 'full', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
