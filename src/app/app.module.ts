import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DropdownComponent } from './component/content/dropdown/dropdown.component';
import { HomeComponent } from './component/home/home.component';
import { ActiveDirective } from './helpers/directives/Active';
import { AccordionComponent } from './component/content/accordion/accordion.component';
import { DropdownDirective } from './directives/dropdown/dropdown.directive';

@NgModule({
  declarations: [
    AppComponent,
    DropdownComponent,
    HomeComponent,
    ActiveDirective,
    AccordionComponent,
    DropdownDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
