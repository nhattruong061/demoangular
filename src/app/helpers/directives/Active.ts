import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
    selector: '[active]'
})

export class ActiveDirective {
    @HostBinding('class.active') isActive: boolean;

    @HostListener('click') activeClick() {
        this.isActive = !this.isActive;
    }
}